package netid.iastate.edu.calculatorapp;

import android.content.Context;
import android.content.res.Resources;

public class CalculatorModel {
    /**
     * A string builder to represent the first number entered in the series of entries (left hand
     * side)
     */
    private StringBuilder mExpression1;
    /**
     * A string builder to represent the second number entered in the series of entries (right hand
     * side)
     */
    private StringBuilder mExpression2;
    /**
     * A string to represent the last operator performed.  null is used to indicate that the first
     * expression is not complete yet.
     */
    private String mOperator = null;
    /**
     * Whether the calculator is in an error state from a number exceeding the max length.  Can only
     * be reset by a clear() action.
     */
    private boolean mIsErrorState = false;
    /**
     * The maximum value to display before triggering an error state.
     */
    private static final int MAX_DISPLAY_LEN = 9;

    /**
     * Constructor.  By default, both expressions are empty and there is no operator.
     */
    CalculatorModel() {
        // Main Strings to represent the expressions
        mExpression1 = new StringBuilder();
        mExpression2 = new StringBuilder();
    }

    /**
     * This method appends to string builders the buttons that are pressed.  It chooses which
     * expression to modify based on the value of mOperator.
     * @param button that was pressed as a string; either a number or a decimal
     */
    public void numberPress(String button) {
        if (mIsErrorState) {
            // Do nothing in an error state.
            return;
        }
        StringBuilder activeExpression;
        if (mOperator == null) {
            // mExpression1 is active
            activeExpression = mExpression1;
        } else {
            // mExpression2 is active
            activeExpression = mExpression2;
        }
        if (button.equals(".")) {
            if (activeExpression.length() == 0) {
                // Expression is empty.  It should be updated to "0."
                activeExpression.append("0.");
            } else if (!activeExpression.toString().contains(".")) {
                // Expression does not contain a decimal already.  It may be added.
                activeExpression.append(".");
            }
            // If the above are not true, the expression already contains a decimal and this one
            // should be ignored.
        } else {
            // Button is a number.  Simply append it.
            activeExpression.append(button);
        }
    }

    /**
     * This method accepts an operator press.  If there's no existing operator, the active
     * expression is changed from mExpression1 to mExpression2.  Otherwise, the pending operator is
     * applied to mExpression1 and mExpression2, and the result is stored in mExpression1.
     * @param button the operator ('/', '*', '+', or '-') that was pressed
     */
    public void operatorPress(String button) {
        if (mIsErrorState) {
            // Do nothing in an error state.
            return;
        }
        // If there is a pending operator, do the calculation to clear out the operator.
        if (mOperator != null) {
            equalsPress();
        }
        // This is done on the assumption that "button" is a valid operator.
        mOperator = button;
    }

    /**
     * Does the pending operation on the two expressions.  Stores the result in expression 1, and
     * clears the operator.
     */
    public void equalsPress() {
        if (mIsErrorState || mOperator == null) {
            // Do nothing in an error state or when there's no operator.
            return;
        }
        // Convert strings to doubles
        double exp1 = getExpression1Value();
        double exp2 = getExpression2Value();
        double result;
        // Do the calculation
        switch (mOperator) {
            case "-":
                result = exp1 - exp2;
                break;
            case "+":
                result = exp1 + exp2;
                break;
            case "*":
                result = exp1 * exp2;
                break;
            case "/":
                result = exp1 / exp2;
                break;
            default:
                // This should never happen.
                return;
        }
        if (Double.isInfinite(result)) {
            // Infinite result, typically from division by 0.
            mIsErrorState = true;
        }
        // Put the result in mExpression1, clear mExpression2, and clear the operator.
        mExpression1 = new StringBuilder(Double.toString(result));
        mExpression2 = new StringBuilder();
        mOperator = null;
    }

    /**
     * Gets the current text to display on the screen of the calculator.  This is either the active
     * expression or "ERROR".  Calling this function may trigger an error condition, if the text is
     * too long for the display.
     * @param activityContext the Activity with the display (used to load a String resource)
     * @return a string of the current display
     */
    public String getDisplay(Context activityContext) {
        if (mIsErrorState) {
            return activityContext.getString(R.string.error);
        }
        String activeExpression;
        if (mOperator == null) {
            // Expression 1 is active
            activeExpression = mExpression1.toString();
        } else {
            // Expression 2 is active
            activeExpression = mExpression2.toString();
        }
        // Check the display length
        if (activeExpression.length() > MAX_DISPLAY_LEN) {
            // Trigger an error state when the display is too long.
            mIsErrorState = true;
            return activityContext.getString(R.string.error);
        }
        // Expand an empty expression to "0".
        if (activeExpression.length() == 0) {
            activeExpression = "0";
        }
        return activeExpression;
    }

    /**
     * Reset the calculator's state.  Clears an error, if there is one.  Resets both expressions to
     * empty.  And, clears the operator, if there is one.
     */
    public void clear() {
        // Reset the whole calculator's state.
        mIsErrorState = false;
        mExpression1 = new StringBuilder();
        mExpression2 = new StringBuilder();
        mOperator = null;
    }

    // ---- HELPER FUNCTIONS ----

    /**
     * Gets double value of the number in mExpression1.
     * @return the double value of mExpression1
     */
    private double getExpression1Value() {
        // Double.parseDouble throws an exception on an empty string.
        if (mExpression1.length() == 0) {
            return 0.0;
        }
        return Double.parseDouble(mExpression1.toString());
    }

    /**
     * Gets double value of the number in mExpression2.
     * @return the double value of mExpression2
     */
    private double getExpression2Value() {
        // Double.parseDouble throws an exception on an empty string.
        if (mExpression2.length() == 0) {
            return 0.0;
        }
        return Double.parseDouble(mExpression2.toString());
    }
}
