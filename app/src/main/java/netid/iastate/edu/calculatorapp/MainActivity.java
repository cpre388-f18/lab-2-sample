package netid.iastate.edu.calculatorapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    /**
     * The instance of the calculator model for use by this controller.
     */
    private CalculatorModel mCalculatorModel;
    /*
     * The instance of the calculator display TextView. You can use this to update the calculator
     * display.
     */
    TextView mCalculatorDisplay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Construct a calculator model
        mCalculatorModel = new CalculatorModel();

        // Locate the instance of the calculator display TextView.  Don't forget to set the ID in
        // the layout file.
        mCalculatorDisplay = findViewById(R.id.displayTextView);

        // Update the UI with the initial state of the mCalculatorModel
        updateUI();
    }

    // ---- BUTTON EVENT HANDLERS ----

    public void onZeroClicked(View view) {
        mCalculatorModel.numberPress("0");
        updateUI();
    }

    public void onOneClicked(View view) {
        mCalculatorModel.numberPress("1");
        updateUI();
    }

    public void onTwoClicked(View view) {
        mCalculatorModel.numberPress("2");
        updateUI();
    }

    public void onThreeClicked(View view) {
        mCalculatorModel.numberPress("3");
        updateUI();
    }

    public void onFourClicked(View view) {
        mCalculatorModel.numberPress("4");
        updateUI();
    }

    public void onFiveClicked(View view) {
        mCalculatorModel.numberPress("5");
        updateUI();
    }

    public void onSixClicked(View view) {
        mCalculatorModel.numberPress("6");
        updateUI();
    }

    public void onSevenClicked(View view) {
        mCalculatorModel.numberPress("7");
        updateUI();
    }

    public void onEightClicked(View view) {
        mCalculatorModel.numberPress("8");
        updateUI();
    }

    public void onNineClicked(View view) {
        mCalculatorModel.numberPress("9");
        updateUI();
    }

    public void onDecimalClicked(View view) {
        mCalculatorModel.numberPress(".");
        updateUI();
    }

    public void onPlusClicked(View view) {
        mCalculatorModel.operatorPress("+");
        updateUI();
    }

    public void onMinusClicked(View view) {
        mCalculatorModel.operatorPress("-");
        updateUI();
    }

    public void onTimesClicked(View view) {
        mCalculatorModel.operatorPress("*");
        updateUI();
    }

    public void onDivideClicked(View view) {
        mCalculatorModel.operatorPress("/");
        updateUI();
    }

    public void onEqualClicked(View view) {
        mCalculatorModel.equalsPress();
        updateUI();
    }

    public void onClearClicked(View view) {
        mCalculatorModel.clear();
        updateUI();
    }

    // ---- HELPER FUNCTIONS ----

    /**
     * Updates the UI (calculator display) based on the current state of the calculatorModel.
     */
    private void updateUI() {
        mCalculatorDisplay.setText(mCalculatorModel.getDisplay(this));
    }
}
